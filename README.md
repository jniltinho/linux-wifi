# Drivers para placas Wifi USB Linux

## Driver T4Uv3 TP-Link Archer T4U AC1300

```bash
## Run as root (sudo su)
## Ubuntu 18.04
## https://www.tp-link.com/ae/support/download/archer-t4u/#Driver

wget -O T4Uv3_WiFi_linux_v5.3.1_beta.zip https://static.tp-link.com/2018/201810/20181018/Archer%20T4U_V3_181018_Linux_beta.zip
unzip T4Uv3_WiFi_linux_v5.3.1_beta.zip
cd T4Uv3_WiFi_linux_v5.3.1_beta/
make clean
make
make install
depmod -a
modprobe 88x2bu
```
